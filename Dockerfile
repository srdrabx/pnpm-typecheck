# Use the official Node.js 20 Alpine image as a base
FROM node:20-alpine

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy all the content
COPY . .

ENV CI=true

ENV GITLAB_CI=true

ENV GITLAB=true

# Install pnpm
RUN npm install -g pnpm@8.6.11

# Install dependencies using pnpm
RUN pnpm install --frozen-lockfile

# The command to run your application
CMD ["pnpm", "run", "--workspace-concurrency", "1", "-r", "typecheck"]
